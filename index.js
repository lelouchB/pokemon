import React, { Component } from 'react';
import { render } from 'react-dom';
import html2canvas from 'html2canvas'
import './style.css';

function App() {

  const [query, setQuery] = React.useState("bulbasaur");
  const [pokemon, setPokemon] = React.useState(null);
  const [hasError, setHasError] = React.useState(false);

  React.useEffect(() => {
    getPokemon(query);
  }, []);

  async function getPokemon(name) {
    setHasError(false);

    try {
      const res = await fetch(`https://pokeapi.co/api/v2/pokemon/${name}`);
      const data = await res.json();
      setPokemon(data);
    } catch (err) {
      setHasError("no pokemon found");
    }
  }

  function handleSubmit(e) {
    e.preventDefault();
    getPokemon(query);
  }

  function handleChange(e) {
    setQuery(e.target.value);
    getPokemon(e.target.value);
  }

  return (
    <div>
      <form onSubmit={handleSubmit}>
        <input
          type="text"
          placeholder="Search the Pokemons..."
          value={query}
          onChange={handleChange}
        />
        <input type="submit" value="Search" />
      </form>

      {hasError && "no pokemon found"}
      {!hasError && pokemon && <Pokemon pokemon={pokemon} />}

    </div>
  );
}

function Pokemon({ pokemon }) {
  return (
    
    <div className="pokemon">

      <div className="info">
        <img
          src={`https://pokeres.bastionbot.org/images/pokemon/${pokemon.id}.png`}
          width="200"
        />
        <h2>{pokemon.name}</h2>
      </div>

      <div className="stats">
        {pokemon.stats.map((stat, index) => (
          <p key={index}>
            {stat.stat.name}: {stat.base_stat}
          </p>
        ))}
      </div>
    </div>
  );
}
render(<App />, document.getElementById("app"));
